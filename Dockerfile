FROM openjdk:17-oracle
MAINTAINER nik
COPY build/libs/middle01userservice1-0.0.1-SNAPSHOT.jar userservice.jar
ENTRYPOINT ["java", "-jar", "userservice.jar"]